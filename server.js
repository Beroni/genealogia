const http = require("http");
require("dotenv").config();
const PORT = process.env.APP_PORT || 3000;
const app = require("./app");
const server = http.createServer(app);

//
server.listen(PORT, () => console.log(`Listening on ${PORT}`));
