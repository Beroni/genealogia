const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const fs = require("fs");
const axios = require("axios");
const root_dir = process.cwd();
const dataManagerUrl = process.env.DATA_MANAGER_URL || process.env.DEFAULT_DM_URL;

router.get("/", (req, res) => {
    console.log("get heatmap realizado.");
    res.render(root_dir + "/src/heatmap/index.html");
    
});



router.get("/:rfid", (req, res) => {
    axios
    .get(dataManagerUrl + "/rfid/?id=" + req.params.rfid)
    .then(response => {
        if(response){
        res.send(response.data)
    }else{
        const json = {
            data: "Family Not Registered"
        };
    }
    })
    .catch(err => res.send(err));

});


router.get("/family/:surname", (req, res) =>{
    axios
        .get(dataManagerUrl + "/locations/?surname=" + encodeURI(req.params.surname))
        .then(response => {
            if(response){
            res.send({
                data: response.data,
                image: "brasaogirao.jpg"
            })
        }else{
            const json = {
                data: "Family Not Found"
            };
        }
        })
        .catch(err => res.send(err));
});

module.exports = router;
