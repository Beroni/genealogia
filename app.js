const express = require("express");
const engines = require("consolidate");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");

// Routes
const heatmapRoutes = require("./routes/heatmap");
const genealogyRoutes = require("./routes/genealogy");
const battleRoutes = require("./routes/battle");
const resumeRoutes = require("./routes/resume");
const regionsRoutes = require("./routes/regions");
const rfidRoutes = require("./routes/rfid");

app.use(cors());
app.use(express.static("public"));
app.engine("html", engines.mustache);

app.set("view engine", "html");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/heatmap", heatmapRoutes);
app.use("/genealogy", genealogyRoutes);
app.use("/battle", battleRoutes);
app.use("/resume", resumeRoutes);
app.use("/regions", regionsRoutes);
app.use("/rfid", rfidRoutes);

module.exports = app;
