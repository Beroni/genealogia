let searchButton = document.querySelector("#submit-form-search");
let inputInfo = document.querySelector("#search-family");
let arrowButton = document.querySelector("#return-search");
let familyNameInfo = document.querySelector("#familyNameInfo");

let placeList = [
  "Arneiroz",
  "Assaré",
  "Aurora",
  "Baixio",
  "Cairiaçu",
  "Cariús",
  "Cedro",
  "Crato",
  "Farias Brito",
  "Granjeiro",
  "Icó",
  "Iguatu",
  "Ipaumirim",
  "Jaguaribe",
  "Juazeiro do Norte",
  "Jucás",
  "Lavras da Mangabeira",
  "Missão Velha",
  "Orós",
  "Parambu",
  "Quixelô",
  "Saboeiro",
  "Tauá",
  "Umari",
  "Várzea Alegre"
];

function testAnim(x) {
  $(".modal .modal-dialog").attr("class", "modal-dialog  " + x + "  animated");
}

function getInfo(surname) {
  return new Promise((resolve, reject) => {
    try {
      $.ajax({
        type: "GET",
        url: "/resume/family/" + surname,
        dataType: "json",
        success: res => {
          if (res.data === "Family Not Found") {
            $("#search-form").addClass("has-error");
            $("#search-family").attr(
              "placeholder",
              "Digite o nome da família corretamente."
            );
          } else {
            resolve(res.data);
          }
        },
        error: err => {
          $("#search-form").addClass("has-error");
          $("#search-family").attr(
            "placeholder",
            "Digite o nome da família corretamente."
          );
          reject(Error(err));
        }
      });
    } catch (err) {
      return reject(Error(err));
    }
  });
}
searchButton.onclick = async () => {
  try {
    if (family.surnames.includes(inputInfo.value)) {
      loadingBar();
      let info = await getInfo(inputInfo.value);
      console.log(info);
      handleInfo(info);
    } else {
      $("#search-form").addClass("has-error");
      $("#search-family").attr(
        "placeholder",
        "Digite o nome da família corretamente."
      );
    }
  } catch (err) {}
};

function loadingBar() {
  $("#loading").html(`
    <div class="loader"></div>
    `);
}

function removeLoadingBar() {
  $("#loading").empty();
}

function handleInfo(info) {
  $("#search-family").val("");
  myKeyboard.setInput("");
  $("#youngest").empty();
  $("#oldest").empty();
  $("#familyNameInfo").empty();
  $("#memberNumInfo").empty();
  $("#imageBrasao").empty();
  if (info != null) {
    $("#searchModal").modal("hide");
    $("#infoModal").removeClass("hidden");
    $("#infoModal").addClass("visible");
    if (info.topcities && info.topcities.length != 0) {
      try {
        if (placeList.includes(info.topcities[0].city)) {
          $("#predominantRegion").attr(
            "src",
            "./familyresume_experiment/regions/" +
              info.topcities[0].city +
              ".svg"
          );
        } else {
          $("#predominantRegion").attr(
            "src",
            "./familyresume_experiment/regions/Regiao_nao_encontrada.svg"
          );
        }
      } catch {
        $("#predominantRegion").attr(
          "src",
          "./familyresume_experiment/regions/Regiao_nao_encontrada.svg"
        );
      }
    } else {
      $("#predominantRegion").attr(
        "src",
        "./familyresume_experiment/regions/Regiao_nao_encontrada.svg"
      );
    }
    info.oldest.forEach(element => {
      let ul = document.getElementById("oldest");
      let li = document.createElement("li");
      li.appendChild(
        document.createTextNode(element.person + " - " + element.year)
      );
      ul.appendChild(li);
    });

    info.youngest.forEach(element => {
      let ul = document.getElementById("youngest");
      let li = document.createElement("li");
      li.appendChild(
        document.createTextNode(element.person + " - " + element.year)
      );
      ul.appendChild(li);
    });

    $("#imageBrasao").append(
      `<strong id="familyLetter"style="font-size:67px;">${info.surname[0]}</strong>`
    );

    $("#familyNameInfo").html(
      `
        <h2>
        ${info.surname}
        </h2>
        <h4>
        Desde ${info.oldest[0].year}
        </h4>
        `
    );

    $("#memberNumInfo").html(
      `
        <div style="    padding-left: 5px;">
        <h3>
            Aproximadamente
        </h3>
        <h3>
        ${
          info.totalPopulation == 1
            ? info.totalPopulation + " membro"
            : info.totalPopulation + " membros"
        } 
        </h3>
        </div>
        `
    );
  } else {
    $("#search-form").addClass("has-error");
    $("#search-family").attr(
      "placeholder",
      "Digite o nome da família corretamente."
    );
  }
  removeLoadingBar();
}

arrowButton.onclick = () => {
  $("#infoModal").removeClass("visible");
  $("#infoModal").addClass("hidden");
  $("#searchModal").modal("show");
};
