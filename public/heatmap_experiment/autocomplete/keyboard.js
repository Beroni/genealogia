let Keyboard = window.SimpleKeyboard.default;


const pt_br = {
    'default': [
        '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
        '{tab} q w e r t y u i o p ´ [',
        '{lock} a s d f g h j k l ç ~ ] {enter}',
        '{shift} z x c v b n m , . ; / {shift}',
        '.com @ {space}'
    ],
    'shift': [
        '" ! @ # $ % ¨ & * ( ) _ + {bksp}',
        '{tab} Q W E R T Y U I O P ` {',
        '{lock} A S D F G H J K L Ç ^ } {enter}',
        '{shift} Z X C V B N M < > : ? {shift}',
        '.com @ {space}'
    ]
};

let myKeyboard = new Keyboard({
    onChange: input => onChange(input),
    onKeyPress: button => onKeyPress(button),
    physicalKeyboardHighlight: true,
    physicalKeyboardHighlightTextColor: "white",
    layout: pt_br
});

document.querySelector(".input").addEventListener("input", event => {
    myKeyboard.setInput(event.target.value);
});

function onChange(input) {
    let text = myKeyboard.getInput();
    let lastLetter = text.charAt(text.length - 1);
    let penultimateLetter = text.charAt(text.length - 2);
    
    switch (penultimateLetter) {
        case '~':
            if (lastLetter === "a") handleAccent("ã");
            else if (lastLetter === "e") handleAccent("ẽ");
            else if (lastLetter === "i") handleAccent("ĩ");
            else if (lastLetter === "o") handleAccent("õ");
            else if (lastLetter === "u") handleAccent("ũ");
            else if (lastLetter === "A") handleAccent("Ã");
            else if (lastLetter === "E") handleAccent("Ẽ");
            else if (lastLetter === "I") handleAccent("Ĩ");
            else if (lastLetter === "O") handleAccent("Õ");
            else if (lastLetter === "U") handleAccent("Ũ");
            break;
        case '´':
            if (lastLetter === "a") handleAccent("á");
            else if (lastLetter === "e") handleAccent("é");
            else if (lastLetter === "i") handleAccent("í");
            else if (lastLetter === "o") handleAccent("ó");
            else if (lastLetter === "u") handleAccent("ú");
            else if (lastLetter === "A") handleAccent("Á");
            else if (lastLetter === "E") handleAccent("É");
            else if (lastLetter === "I") handleAccent("Í");
            else if (lastLetter === "O") handleAccent("Ó");
            else if (lastLetter === "U") handleAccent("Ú");
            break;
        case '^':
            if (lastLetter === "a") handleAccent("â");
            else if (lastLetter === "e") handleAccent("ê");
            else if (lastLetter === "i") handleAccent("î");
            else if (lastLetter === "o") handleAccent("ô");
            else if (lastLetter === "u") handleAccent("û");
            else if (lastLetter === "A") handleAccent("Â");
            else if (lastLetter === "E") handleAccent("Ê");
            else if (lastLetter === "I") handleAccent("Î");
            else if (lastLetter === "O") handleAccent("Ô");
            else if (lastLetter === "U") handleAccent("Û");
            break;
        case '`':
            if (lastLetter === "a") handleAccent("à");
            else if (lastLetter === "e") handleAccent("è");
            else if (lastLetter === "i") handleAccent("ì");
            else if (lastLetter === "o") handleAccent("ò");
            else if (lastLetter === "u") handleAccent("ù");
            else if (lastLetter === "A") handleAccent("À");
            else if (lastLetter === "E") handleAccent("È");
            else if (lastLetter === "I") handleAccent("Ì");
            else if (lastLetter === "O") handleAccent("Ò");
            else if (lastLetter === "U") handleAccent("Ù");
            break;
        case '¨':
            if (lastLetter === "a") handleAccent("ä");
            else if (lastLetter === "e") handleAccent("ë");
            else if (lastLetter === "i") handleAccent("ï");
            else if (lastLetter === "o") handleAccent("ö");
            else if (lastLetter === "u") handleAccent("ü");
            else if (lastLetter === "A") handleAccent("Ä");
            else if (lastLetter === "E") handleAccent("Ë");
            else if (lastLetter === "I") handleAccent("Ï");
            else if (lastLetter === "O") handleAccent("Ö");
            else if (lastLetter === "U") handleAccent("Ü");
            break;
        default:
            document.querySelector(".input").value = input;
            break;
    }

}

function onKeyPress(button) {
    if (button === "{shift}" || button === "{lock}") handleShift();
    if (button === "{bksp}") {
        let text = myKeyboard.getInput();
        let newText = text.substring(0, text.length - 1);
        myKeyboard.setInput(newText);
        document.querySelector(".input").value = newText
    }
    if (button === "{enter}") {
        handleFamilyName($('input[name=familyname]').val());
        $("#search-family").val("");
    }

}

function handleFamilyName(name) {
    familyName = {
        family: name
    };
    sendFamilyName(familyName);
}
function handleAccent(accent) {
    let text = myKeyboard.getInput();
    let newText = text.substring(0, text.length - 2) + accent;
    myKeyboard.setInput(newText);
    document.querySelector(".input").value = newText;
}

function handleShift() {
    let currentLayout = myKeyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";
    myKeyboard.setOptions({
        layoutName: shiftToggle
    });
    
}

async function getFamilyNamebyRFID(tag){
    return new Promise((resolve, reject) => {
        try {
                $.ajax({
                    type: 'GET',
                    url: '/rfid/'+encodeURI(String(tag)),
                    dataType: 'json',
                    success: (res) => {

                        return resolve(res.name);
                    },
                    error:  (err) => {
                        return reject(Error(err));
                    }
                });
        } catch (err) {
            return reject(Error("Error"));
        }
    });
}

function rfidEnter(event){
    let char = event.which || event.keyCode;
    if (char === 13) {
        event.preventDefault();
        if (!isModalOpen())
            openModal();
        removeMarkers();
        var a = $('input[name=familynamerfid]').val()
        getFamilyNamebyRFID(a)
            .then(f_name =>{ 
                handleFamilyName(f_name);
                $("#search-family-rfid").val("");
            })
            .catch((err) => console.log(err));
    }
}
$(".hg-button").on('click',() =>{
    $("#search-family").autocomplete("search",  myKeyboard.getInput());
});

