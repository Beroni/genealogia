let inputInfo = document.querySelector("#search-family");

let returnBtn = document.querySelector("#search-form-button");

let Voltar = document.querySelector("#card-fade-back");

returnBtn.onclick = () => {
  let displayTree = document.querySelector("#Arvore").style.display;

  if (displayTree === "none") {
    $("#personSelectModal").css("visibility", "hidden");
  } else {
    $("#Arvore").css("display", "none");
  }
  $(".list-group").empty();
  $("#search-form-button").removeClass("m-fadeIn");
  $("#search-form-button").addClass("m-fadeOut");
  $("#myModal").modal("show");
  $("#personSelectModal").css("visibility", "hidden");
};

$("#submit-form-search").click(async () => {
  let surname = inputInfo.value.trim();
  if (surname.length < 3 && surname != "Sá") {
    return;
  }
  let year = $("#yearPicker").val();
  let place = $("#placePicker").val();
  loadingBar();
  let persons = await getPersons(
    surname,
    year !== "-" ? year : "",
    place !== "-" ? place : ""
  );
  $("#search-form-button").addClass("m-fadeIn");
  $("#search-form-button").removeClass("m-fadeOut");
  removeLoadingBar();
  if (persons.length > 0) {
    persons.forEach((obj, index) => {
      let listTemplate = `
               <div onclick="pickID(this)" value="${
                 obj.id
               }" class="list-group-item" style="width:1320px; margin-top:7px;">
                    <div>
                        <h2>
                            ${obj.prefix || ""}
                            ${obj.given || ""}
                        </h2>
                        <h3>
                            ${obj.surname || ""}
                            ${obj.suffix || ""}
                        </h3>
                        <h3>
                            ${obj.facts || ""}
                        </h3>
                    </div>
                </div>
                `;
      $(".list-group").append(listTemplate);
    });
  } else {
    let errorMessage = `
        <strong style="font-size: 25px">
              Nenhum resultado foi encontrado para o critério de busca utilizado:
              <br />
              Sobrenome: ${surname}
              <br />
              Local: ${place}
              <br />
              Ano: ${year}
              <br />
              Por favor, verifique se os nomes digitados possuem algum erro de
              ortografia ou tente um critério de busca diferente.
        </strong>
    `;
    $(".list-group").append(errorMessage);
  }
  $("#myModal").modal("hide");
  $("#personSelectModal").css("visibility", "visible");
});

function getFamilyTree(id) {
  return new Promise((resolve, reject) => {
    try {
      $.ajax({
        type: "GET",
        url: "/genealogy/id/" + id,
        dataType: "json",
        success: res => {
          resolve(res);
        },
        error: err => {
          reject(Error(err));
        }
      });
    } catch (err) {
      return reject(Error(err));
    }
  });
}

function getPersons(surname, year, place) {
  return new Promise((resolve, reject) => {
    try {
      $.ajax({
        type: "POST",
        url: "/genealogy/family/",
        data: {
          surname: surname,
          year: year,
          place: place
        },
        dataType: "json",
        success: res => {
          resolve(res);
        },
        error: err => {
          reject(Error(err));
        }
      });
    } catch (err) {
      return reject(Error(err));
    }
  });
}

Voltar.onclick = () => {
  $("#card-fade-back").addClass("m-fadeOut");
  $("#card-fade-back").removeClass("m-fadeIn");

  $("#search-form-button").addClass("m-fadeIn");
  $("#search-form-button").removeClass("m-fadeOut");

  $("#personSelectModal").css("visibility", "visible");
  $("#Arvore").css("visibility", "hidden");

  $(".pai").empty();
  $(".mae").empty();
  $(".pessoa").empty();
  $(".filhos").empty();
  $(".conjuge").empty();
};

async function pickID(elm) {
  var id = elm.getAttribute("value");

  let familyTree = await getFamilyTree(id);

  $("#search-form-button").removeClass("m-fadeIn");
  $("#search-form-button").addClass("m-fadeOut");
  $("#card-fade-back").addClass("m-fadeIn");
  $("#card-fade-back").removeClass("m-fadeOut");

  $("#personSelectModal").css("visibility", "hidden");

  // $(".list-group").empty();
  $(".pai").empty();
  $(".mae").empty();
  $(".pessoa").empty();
  $(".filhos").empty();
  $(".conjuge").empty();
  var familia = familyTree.families;

  document.getElementById("Arvore").style = "display:inline; ";
  $(".list-group");
  if (familyTree.father) {
    let listTemplate = `
               <li onclick="pickID(this)"  value="${familyTree.father.id}" >
                    <div>
                    
                        <h2 style="text-align: center;">
                            ${familyTree.father.prefix || ""}
                            ${familyTree.father.given || ""}
                        </h2>
                        <h3 style="text-align: center;">
                            ${familyTree.father.surname || ""}
                            ${familyTree.father.suffix || ""}
                        </h3>
                        <h3 style="margin-top: 10px; text-align: center;">
                            ${familyTree.father.facts || ""}
                        </h3>
                        
                    </div>
                </li>
                `;
    $(".pai").append(listTemplate);

    $("#myModal").modal("hide");
    $("#Arvore").css("visibility", "visible");
  }
  if (familyTree.mother) {
    listTemplate = `
              <li onclick="pickID(this)" value="${familyTree.mother.id}" >
                    <div>
                    
                        <h2 style="text-align: center;">
                            ${familyTree.mother.prefix || ""}
                            ${familyTree.mother.given || ""}
                        </h2>
                        <h3 style="text-align: center;">
                            ${familyTree.mother.surname || ""}
                            ${familyTree.mother.suffix || ""}
                        </h3>
                        <h3 style="margin-top: 10px; text-align: center;">
                            ${familyTree.mother.facts || ""}
                        </h3>
                    </div>
                </li>
                `;
    $(".mae").append(listTemplate);
    $("#myModal").modal("hide");
    $("#Arvore").css("visibility", "visible");
  }
  listTemplate = `
   
               <li  value="${familyTree.id}"  >
                
                    <div>
                    
                        <h2 style="text-align: center;">
                            ${familyTree.prefix || ""}
                            ${familyTree.given || ""}
                        </h2>
                        <h3 style="text-align: center;">
                            ${familyTree.surname || ""}
                            ${familyTree.suffix || ""}
                        </h3>
                        <h3 style="margin-top: 10px; text-align: center;">
                            ${familyTree.facts || ""}
                        </h3>
                        
                    </div>
                   
                </li>
                `;
  $(".pessoa").append(listTemplate);

  $("#myModal").modal("hide");
  $("#Arvore").css("visibility", "visible");
  document.getElementById("filhos").style.display = "none";
  document.getElementById("conjuge").style.display = "none";
  if (familia) {
    for (var j = 0; j < familia.length; j++) {
      if (familia[j].children) {
        document.getElementById("filhos").style.display = "inline";
        for (i = 0; i < familia[j].children.length; i++) {
          listTemplate = `
               <li onclick="pickID(this)"  value="${
                 familia[j].children[i].id
               }" class="list-group-item2" style="display:inline-block;border-radius: 0px;vertical-align: top;">
               <div style="left:0px; width: 363px;height: 203px;">
                        <h2 style="text-align: center;">
                            ${familia[j].children[i].prefix || ""}
                            ${familia[j].children[i].given || ""}
                        </h2>
                        <h3 style="text-align: center;">
                            ${familia[j].children[i].surname || ""}
                            ${familia[j].children[i].suffix || ""}
                        </h3>
                        <h3 style="margin-top: 10px; text-align: center; white-space: normal;">
                            ${familia[j].children[i].facts || ""}
                        </h3>
                        
                    </div>
                    <div style=" width: 10px;height: 203px;"></div>
                </li>
                
                `;
          $(".filhos").append(listTemplate);

          $("#myModal").modal("hide");
          $("#Arvore").css("visibility", "visible");
        }
      }

      if (familia[j].spouse) {
        document.getElementById("conjuge").style.display = "inline";

        listTemplate = `
            
            <li onclick="pickID(this)"  value="${
              familia[j].spouse.id
            }" class="list-group-item2" style="display:inline-block;border-radius: 0px;">
                    
            <div style="left:0px; width: 302px;height: 180px; ">
                    
                        <h2 style="text-align: center;">
                            ${familia[j].spouse.prefix || ""}
                            ${familia[j].spouse.given || ""}
                        </h2>
                        <h3 style="text-align: center;">
                            ${familia[j].spouse.surname || ""}
                            ${familia[j].spouse.suffix || ""}
                        </h3>
                        <h3 style="margin-top: 10px; text-align: center;">
                            ${familia[j].spouse.facts || ""}
                        </h3>
                        
                    </div>
                    
                </li>
                `;
        $(".conjuge").append(listTemplate);

        $("#myModal").modal("hide");
        $("#Arvore").css("visibility", "visible");
      }
    }
  }
}

function loadingBar() {
  $("#loading").html(`
    <div class="loader"></div>
    `);
}

function removeLoadingBar() {
  $("#loading").empty();
}
