var cont = 0;
var numero = 0;
var ganhador = "";
var gan = Array(5);
var name1 = "";
var name2 = "";
var barras = Array(3);
var value = Array(2);
//Coelho: 2572785670
//Cabral: 1098594795
//Silva: 1870009314
var color = Array(2);
var posicao = "";
var text = document.getElementById("textt");
var id1 = "";
var id2 = "";
var c = 0;

function mostrar_resultado() {
  document.getElementById("vitoria").style.display = "none";
}
text.addEventListener("keypress", function (evento) {
  if (evento.key == "Enter" && c == 0) {
    // console.log(id1);

    ENTER("family1", id1, 1);
    id1 = "";
  } else if (evento.key == "Enter") {
    // console.log(id2);

    ENTER("family2", id2, 2);
    id2 = "";
  }
  if (c == 0 && evento.key != "Enter") {
    id1 += evento.key;
  }
  if (c == 1 && evento.key != "Enter") {
    id2 += evento.key;
  }
});
function ENTER(ID, v, id) {
  if (name2 == "")
    getData(v).then((response) => {
      nome(ID, response);
    });
}
function nome(ID, response) {
  if (response != null) {
    c++;
    //document.getElementById(ID).style.opacity = 0;

    if (numero == 0) {
      document.getElementById(ID + "numero").innerHTML = "I";
      document.getElementById("explica").style.display = "none";
      numero++;
      if (ID == "family1") {
        name1 = response;
      } else {
        name2 = response;
      }
    } else if (document.getElementById(ID + "numero").innerHTML == "O") {
      numero++;
      document.getElementById(ID + "numero").innerHTML = "II";

      if (ID == "family1") {
        name1 = response;
      } else {
        name2 = response;
      }
    }

    document.getElementById(ID + "entrada").style.display = "none";
    document.getElementById(ID + "_on_modal").style.display = "none";
    document.getElementById(ID + "infoc").style.display = "inline";

    document.getElementById(ID + "letra").style =
      "position:relative; z-index: 1050;left: 87px;top: -76px; font-style: normal; font-size: 60pt;";
    document.getElementById(ID + "letra").innerHTML = response[0];
    document.getElementById(ID + "imagem").src = document.getElementById(
      "family1imagem"
    ).src;
    var SRC = document.getElementById("family1imagem").src;
    while (SRC == document.getElementById(ID + "imagem").src) {
      // console.log(ID)
      var temp = parseInt(Math.random() * 4);
      // console.log(temp)
      if (temp == 0) {
        //vermelho
        document.getElementById(ID + "imagem").src =
          "./battle_experiment/img/brasões/vermelho.svg";
        if (ID == "family1") color[0] = "#b98282";
        else color[1] = "#b98282";
      }
      if (temp == 1) {
        //verde
        document.getElementById(ID + "imagem").src =
          "./battle_experiment/img/brasões/verde.svg";
        if (ID == "family1") color[0] = "#82b9ad";
        else color[1] = "#82b9ad";
      }
      if (temp == 2) {
        //azul
        document.getElementById(ID + "imagem").src =
          "./battle_experiment/img/brasões/azul.svg";
        if (ID == "family1") color[0] = "#88a3c2";
        else color[1] = "#88a3c2";
      }
      if (temp == 3) {
        //amarelo
        document.getElementById(ID + "imagem").src =
          "./battle_experiment/img/brasões/amarelo.svg";
        if (ID == "family1") color[0] = "#d4cd94";
        else color[1] = "#d4cd94";
      }
    }

    document.getElementById(ID + "imagem").style =
      "position:relative; top:120px; left:65px; height:200px; width:200px;";

    document.getElementById(ID + "nome").innerHTML = response;
    document.getElementById(ID + "nome").style =
      "position:relative;top:50px; font-style: normal; font-size: 28pt;";

    document.getElementById(ID + "infon").innerHTML = response;

    function atualizar_textos(quantity, description) {
      document.getElementById(ID + "texto").innerHTML = quantity;
      document.getElementById(ID + "texto").style = "position:relative;";

      document.getElementById(ID + "infot").innerHTML = description;
      document.getElementById(ID + "infot").style.alignContent = "normal";
    }

    if (name1 && ID == "family1") {
      getDatat(name1).then((response) => {
        value[0] = response;
        atualizar_textos(value[0][0].quantity, value[0][0].description);
      });
    }
    if (name2 && ID == "family2") {
      getDatat(name2).then((response) => {
        value[1] = response;
        atualizar_textos(value[1][0].quantity, value[1][0].description);
      });
    }
  }
}

function grafico(ID, DATA, bt) {
  // amarelo rgb(132, 121, 64)  =  #847940
  // azul rgb(45, 144, 168) =  #2d90a8
  // verde rgb(0, 131, 97) =  #82b9ad
  // vermelho rgb(218, 162, 159) = #daa29f
  if (bt == "antiga") {
    DATA[0] = 1920 - DATA[0];
    DATA[1] = 1920 - DATA[1];
  }
  var ctx = document.getElementById(ID).getContext("2d");
  var chart = new Chart(ctx, {
    type: "bar",

    data: {
      labels: ["", ""],
      datasets: [
        {
          label: "",
          data: DATA,
          backgroundColor: color,
          // [
          //     'rgb(197, 172, 0)',
          //     'rgb(122, 80, 0)'
          // ]
          borderColor: color,
          // [
          //     'rgb(122, 80, 0)',
          //     'rgb(122, 80, 0)'
          // ]
          borderWidth: 1,
        },
      ],
    },
    options: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: "",
      },

      scales: {
        yAxes: [
          {
            ticks: {
              display: false,
              beginAtZero: true,
            },
            barPercentage: 10,
            gridLines: {
              stacked: false,
              offsetGridLines: false,
              color: "rgba(0, 0, 0, 0)",
            },
          },
        ],
        xAxes: [
          {
            ticks: {
              display: false,
              beginAtZero: true,
            },

            stacked: false,
            barPercentage: 1,
            gridLines: {
              offsetGridLines: false,
              color: "rgba(0, 0, 0, 0)",
            },
          },
        ],
      },
    },
  });
}
function pop_up() {
  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function () {
    x.className = x.className.replace("show", "");
  }, 3000);
}
async function getDatat(numero) {
  return new Promise((resolve, reject) => {
    try {
      $.ajax({
        type: "GET",
        url: "/battle/name/" + encodeURI(String(numero)),
        dataType: " json",
        success: function (res) {
          return resolve(res.data);
        },
        error: function (err) {
          pop_up();
        },
      });
    } catch (err) {
      return reject(Error("not found"));
    }
  });
}

async function getData(numero) {
  return new Promise((resolve, reject) => {
    // console.log("res")
    try {
      $.ajax({
        type: "GET",
        url: "/battle/" + encodeURI(String(numero)),
        dataType: " json",
        success: function (res) {
          // console.log(res)
          if (res.name == "RFID not found") {
            pop_up();
            return null;
          } else return resolve(res.name);
        },
        error: function (err) {
          return reject(Erro(err));
        },
      });
    } catch (err) {
      return reject(Error("not found"));
    }
  });
}

function btnf(ID) {
  var resu = "";

  if (numero == 2) {
    if (document.getElementById(ID).style.opacity > 0.65 && cont < 3) {
      document.getElementById("esconde").style.display = "inline";
      cont++;
      document.getElementById("family1infoc").style.display = "none";
      document.getElementById("family2infoc").style.display = "none";
      for (var i = 0; i <= 3; i++) {
        if (!barras[i]) {
          barras[i] = ID;
          // console.log(parseInt(value[0][0].totalPopulation))
          // console.log(parseInt(value[0][0].totalPopulation))
          if (ID == "homens") {
            DATA = [
              parseInt(
                (100 * parseInt(value[0][0].totalMales)) /
                  parseInt(value[0][0].totalPopulation)
              ),
              parseInt(
                (100 * parseInt(value[1][0].totalMales)) /
                  parseInt(value[1][0].totalPopulation)
              ),
            ];
            if (
              parseInt(
                (100 * parseInt(value[0][0].totalMales)) /
                  parseInt(value[0][0].totalPopulation)
              ) ==
              parseInt(
                (100 * parseInt(value[1][0].totalMales)) /
                  parseInt(value[1][0].totalPopulation)
              )
            )
              resu = "";
            else if (
              parseInt(
                (100 * parseInt(value[0][0].totalMales)) /
                  parseInt(value[0][0].totalPopulation)
              ) >
              parseInt(
                (100 * parseInt(value[1][0].totalMales)) /
                  parseInt(value[1][0].totalPopulation)
              )
            ) {
              resu = "1";
              gan[0] = 1;
            } else {
              resu = "2";
              gan[0] = 2;
            }
          }
          if (ID == "mulheres") {
            DATA = [
              parseInt(
                (100 * parseInt(value[0][0].totalFemales)) /
                  parseInt(value[0][0].totalPopulation)
              ),
              parseInt(
                (100 * parseInt(value[1][0].totalFemales)) /
                  parseInt(value[1][0].totalPopulation)
              ),
            ];
            if (
              parseInt(
                (100 * parseInt(value[0][0].totalFemales)) /
                  parseInt(value[0][0].totalPopulation)
              ) ==
              parseInt(
                (100 * parseInt(value[1][0].totalFemales)) /
                  parseInt(value[1][0].totalPopulation)
              )
            )
              resu = "";
            else if (
              parseInt(
                (100 * parseInt(value[0][0].totalFemales)) /
                  parseInt(value[0][0].totalPopulation)
              ) >
              parseInt(
                (100 * parseInt(value[1][0].totalFemales)) /
                  parseInt(value[1][0].totalPopulation)
              )
            ) {
              resu = "1";
              gan[1] = 1;
            } else {
              resu = "2";
              gan[1] = 2;
            }
          }
          if (ID == "populosa") {
            DATA = [
              parseInt(value[0][0].totalPopulation),
              parseInt(value[1][0].totalPopulation),
            ];
            if (
              parseInt(value[0][0].totalPopulation) ==
              parseInt(value[1][0].totalPopulation)
            )
              resu = "";
            else if (
              parseInt(value[0][0].totalPopulation) >
              parseInt(value[1][0].totalPopulation)
            ) {
              resu = "1";
              gan[2] = 1;
            } else {
              resu = "2";
              gan[2] = 2;
            }
          }
          if (ID == "antiga") {
            DATA = [
              parseInt(value[0][0].oldestYear),
              parseInt(value[1][0].oldestYear),
            ];
            if (
              1920 - parseInt(value[0][0].oldestYear) ==
              1920 - parseInt(value[1][0].oldestYear)
            )
              resu = "";
            else if (
              1920 - parseInt(value[0][0].oldestYear) >
              1920 - parseInt(value[1][0].oldestYear)
            ) {
              resu = "1";
              gan[3] = 1;
            } else {
              resu = "2";
              gan[3] = 2;
            }
          }
          if (ID == "places") {
            DATA = [
              parseInt(value[0][0].totalPlaces),
              parseInt(value[1][0].totalPlaces),
            ];
            if (
              parseInt(value[0][0].totalPlaces) ==
              parseInt(value[1][0].totalPlaces)
            )
              resu = "";
            else if (
              parseInt(value[0][0].totalPlaces) >
              parseInt(value[1][0].totalPlaces)
            ) {
              resu = "1";
              gan[4] = 1;
            } else {
              resu = "2";
              gan[4] = 2;
            }
          }
          var a = 0;
          var b = 0;
          for (var j = 0; j < gan.length; j++) {
            if (gan[j] == 1) {
              a++;
            }
            if (gan[j] == 2) {
              b++;
            }
          }
          ganhador = "vitoria_";
          if (a > b) {
            ganhador = "family1";
          } else if (b > a) {
            ganhador = "family2";
          }
          var top = 70;
          var aj = "font-size: 18pt; ";
          if (resu == "") {
            aj = "font-style: normal; font-size: 22pt; ";
          }
          if (i == 0) {
            document.getElementById("vencedor1d").style.display = "inline";

            document.getElementById(ID).style =
              "z-index: 5; position:absolute ; top:694px; left:458px;background: transparent; border: 0; opacity: 1;";
            document.getElementById(ID).style.opacity = 0.65;

            if (resu == "1") {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor1").innerHTML = DATA[0] + "%";
              else document.getElementById("nvencedor1").innerHTML = DATA[0];
              document.getElementById("nvencedor1").style =
                "display:inline; position:absolute;top:70px; left:430px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor1").innerHTML =
                value[0][0].surname;
              document.getElementById("tvencedor1").style =
                "display:inline; position:absolute;top:100px; left:430px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";

              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor1").innerHTML = DATA[1] + "%";
              else document.getElementById("nperdedor1").innerHTML = DATA[1];
              document.getElementById("nperdedor1").style =
                "display:inline; position:absolute;top:" +
                top +
                "px; left:575px; font-size: 18pt; height: 20px; width: 200px;";
              document.getElementById("tperdedor1").innerHTML =
                value[1][0].surname;
              document.getElementById("tperdedor1").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:575px; font-size: 18pt; height: 20px; width: 200px;";
            } else {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor1").innerHTML = DATA[0] + "%";
              else document.getElementById("nperdedor1").innerHTML = DATA[0];
              document.getElementById("nperdedor1").style =
                "display:inline; position:absolute;top:" +
                top +
                "px; left:430px; " +
                aj +
                " height: 20px; width: 200px;";
              document.getElementById("tperdedor1").innerHTML =
                value[0][0].surname;
              document.getElementById("tperdedor1").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:430px; " +
                aj +
                " height: 20px; width: 200px;";
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor1").innerHTML = DATA[1] + "%";
              else document.getElementById("nvencedor1").innerHTML = DATA[1];
              document.getElementById("nvencedor1").style =
                "display:inline; position:absolute;top:70px; left:575px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor1").innerHTML =
                value[1][0].surname;
              document.getElementById("tvencedor1").style =
                "display:inline; position:absolute;top:100px; left:575px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
            }
            grafico("myChart1", DATA, ID);
          }
          if (i == 1) {
            document.getElementById("vencedor2d").style.display = "inline";

            document.getElementById(ID).style =
              "z-index: 5; position:absolute ; top:694px; left:815px;background: transparent; border: 0; opacity: 1;";
            document.getElementById(ID).style.opacity = 0.65;
            if (resu == "1") {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor2").innerHTML = DATA[0] + "%";
              else document.getElementById("nvencedor2").innerHTML = DATA[0];
              document.getElementById("nvencedor2").style =
                "display:inline;position:absolute;top:70px; left:785px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor2").innerHTML =
                value[0][0].surname;
              document.getElementById("tvencedor2").style =
                "display:inline; position:absolute;top:100px; left:785px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";

              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor2").innerHTML = DATA[1] + "%";
              else document.getElementById("nperdedor2").innerHTML = DATA[1];
              document.getElementById("nperdedor2").style =
                "display:inline; position:absolute;top:" +
                top +
                "px; left:930px; font-size: 18pt; height: 20px; width: 200px;";
              document.getElementById("tperdedor2").innerHTML =
                value[1][0].surname;
              document.getElementById("tperdedor2").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:930px; font-size: 18pt; height: 20px; width: 200px;";
            } else {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor2").innerHTML = DATA[0] + "%";
              else document.getElementById("nperdedor2").innerHTML = DATA[0];
              document.getElementById("nperdedor2").style =
                "display:inline;position:absolute;top:" +
                top +
                "px; left:785px; " +
                aj +
                " height: 20px; width: 200px;";
              document.getElementById("tperdedor2").innerHTML =
                value[0][0].surname;
              document.getElementById("tperdedor2").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:785px; " +
                aj +
                " height: 20px; width: 200px;";
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor2").innerHTML = DATA[1] + "%";
              else document.getElementById("nvencedor2").innerHTML = DATA[1];
              document.getElementById("nvencedor2").style =
                "display:inline; position:absolute;top:70px; left:930px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor2").innerHTML =
                value[1][0].surname;
              document.getElementById("tvencedor2").style =
                "display:inline; position:absolute;top:100px; left:930px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
            }
            grafico("myChart2", DATA, ID);
          }
          if (i == 2) {
            document.getElementById("vencedor3d").style.display = "inline";

            document.getElementById(ID).style =
              "z-index: 5; position:absolute ; top:694px; left:1175px;background: transparent; border: 0; opacity: 1;";
            document.getElementById(ID).style.opacity = 0.65;
            if (resu == "1") {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor3").innerHTML = DATA[0] + "%";
              else document.getElementById("nvencedor3").innerHTML = DATA[0];
              document.getElementById("nvencedor3").style =
                "display:inline;position:absolute;top:70px; left:1130px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor3").innerHTML =
                value[0][0].surname;
              document.getElementById("tvencedor3").style =
                "display:inline; position:absolute;top:100px; left:1130px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";

              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor3").innerHTML = DATA[1] + "%";
              else document.getElementById("nperdedor3").innerHTML = DATA[1];
              document.getElementById("nperdedor3").style =
                "display:inline; position:absolute;top:" +
                top +
                "px; left:1275px; font-size: 18pt; height: 20px; width: 200px;";
              document.getElementById("tperdedor3").innerHTML =
                value[1][0].surname;
              document.getElementById("tperdedor3").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:1275px; font-size: 18pt; height: 20px; width: 200px;";
            } else {
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nperdedor3").innerHTML = DATA[0] + "%";
              else document.getElementById("nperdedor3").innerHTML = DATA[0];
              document.getElementById("nperdedor3").style =
                "display:inline;position:absolute;top:" +
                top +
                "px; left:1130px; " +
                aj +
                " height: 20px; width: 200px;";
              document.getElementById("tperdedor3").innerHTML =
                value[0][0].surname;
              document.getElementById("tperdedor3").style =
                "display:inline; position:absolute;top:" +
                (top + 30) +
                "px; left:1130px; " +
                aj +
                " height: 20px; width: 200px;";
              if (ID == "homens" || ID == "mulheres")
                document.getElementById("nvencedor3").innerHTML = DATA[1] + "%";
              else document.getElementById("nvencedor3").innerHTML = DATA[1];
              document.getElementById("nvencedor3").style =
                "display:inline; position:absolute;top:40px; left:1275px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
              document.getElementById("tvencedor3").innerHTML =
                value[1][0].surname;
              document.getElementById("tvencedor3").style =
                "display:inline; position:absolute;top:70px; left:1275px; font-style: normal; font-size: 22pt; height: 20px; width: 200px;font-weight:bold;";
            }
            grafico("myChart3", DATA, ID);
          }
          break;
        }
      }
      if (cont == 3) {
        if (ganhador != "vitoria_")
          document.getElementById("vitoria_imagem").style.filter =
            "grayscale(0%)";
        document.getElementById(
          "vitoria_nome"
        ).innerHTML = document.getElementById(ganhador + "nome").innerHTML;
        document.getElementById(
          "vitoria_letra"
        ).innerHTML = document.getElementById(ganhador + "letra").innerHTML;
        document.getElementById("vitoria_imagem").src = document.getElementById(
          ganhador + "imagem"
        ).src;

        document.getElementById("vitoria").style.display = "inline";
        document.getElementById("esconde").style.display = "none";
      }
    } else {
      if (document.getElementById(ID).style.opacity == 0.65) cont--;
      document.getElementById(ID).style.opacity = 1;
      if (ID == "homens") {
        gan[0] = 0;
        document.getElementById(ID).style =
          "z-index: 5; position:relative; top:0px; left:0px;background: transparent; border: 0; opacity: 1;";
      }
      if (ID == "mulheres") {
        gan[1] = 0;
        document.getElementById(ID).style =
          "z-index: 5; position:relative; top:0px; left:0px;background: transparent; border: 0; opacity: 1;";
      }
      if (ID == "populosa") {
        gan[2] = 0;
        document.getElementById(ID).style =
          "z-index: 5; position:relative; top:0px; left:0px; background: transparent; border: 0; opacity: 1;";
      }
      if (ID == "antiga") {
        gan[3] = 0;
        document.getElementById(ID).style =
          "z-index: 5; position:relative; top:0px; left:0px;background: transparent; border: 0; opacity: 1;";
      }
      if (ID == "places") {
        gan[4] = 0;
        document.getElementById(ID).style =
          "z-index: 5; position:relative; top:0px; left:0px;background: transparent; border: 0; opacity: 1;";
      }

      for (i = 0; i < 3; i++) {
        if (barras[i] == ID) {
          barras[i] = "";
          if (i == 0) {
            document.getElementById("vencedor1d").style.display = "none";
          }
          if (i == 1) {
            document.getElementById("vencedor2d").style.display = "none";
          }
          if (i == 2) {
            document.getElementById("vencedor3d").style.display = "none";
          }
          break;
        }
      }
    }
  }
}
function select_family(ID) {
  if (ID == "family1_on_modal") posicao = "family1";
  else if (ID == "family2_on_modal") posicao = "family2";
  else {
    getDatat(document.getElementById("search-family").value).then(
      (response) => {
        console.log(response);
        if (response.length > 0)
          nome(posicao, document.getElementById("search-family").value);
        else pop_up();
        document.getElementById("search-family").value = "";
      }
    );
  }
}
function famili(ID) {
  if (document.getElementById(ID).style.opacity == 0) {
    document.getElementById(ID).style.opacity = 0.65;
    document.getElementById(ID).value = "";
  } else {
    if (document.getElementById(ID).style.opacity == 0.65)
      document.getElementById(ID).style.opacity = 0;
    document.getElementById(ID).value = "";
  }
}
function imagem() {
  document.getElementById(imagem1).style.zIndex = 3;
  document.getElementById(texto1).style.zIndex = 3;
  document.getElementById(imagem1).src = "./img/tela/4.png";
  document.getElementById(texto1).value = "testando";
}
