let slider = document.getElementById("timelineRange");
let output = document.getElementById("slidingbarValue");
 

let layerPontos = L.layerGroup().addTo(map);

let heat = L.heatLayer();

let allDotsStructure = {
    "type": "FeaturesCollection",
    "features": []
};

let control = false;


layerCfg = {
    radius: 40,
    maxZoom: 11,
    blur: 10,
    max: 1.5
};

//attach styles and popups to the marker layer
function highlightDot(e) {
    let layer = e.target;
    layer.setStyle({
        fill: true,
        fillColor: "#ffaf35",
        fillOpacity: 1,
        color: "#501904",
        radius: 10
    });
}

function resetDotHighlight(e) {
    let layer = e.target;
    layer.setStyle({
        fill: true,
        fillColor: "#ffffff",
        fillOpacity: 1,
        color: "#a96f00",
        radius: 5
    });
}

function onEachDot(feature, layer) {

    layer.on({
        mouseover: highlightDot,
        mouseout: resetDotHighlight
    });


    let popup = `
    <table style="width:500px;">
        <tbody>
            <tr>
                <td>
                    <div>
                        <b> Cidade:  </b>
                    </div>
                </td>
                <td>
                    <div>
                        ${feature.properties.city}
                    </div>
                </td>
            </tr>
            <tr>
            <td>
                <div>
                    <b> Localidade:  </b>
                </div>
            </td>
            <td>
                <div>
                    ${feature.properties.name}
                </div>
            </td>
        </tr>
        <tr>
        <td>
            <div>
                <b> Latitude:  </b>
            </div>
        </td>
        <td>
            <div>
                ${feature.geometry.coordinates[1]}
            </div>
        </td>
        </tr>
        <tr>
        <td>
            <div>
                <b> Longitude:  </b>
            </div>
        </td>
        <td>
            <div>
                ${feature.geometry.coordinates[0]}
            </div>
        </td>
    </tr>
    <tr>
    <td>
        <div>
            <b> População Total:  </b>
        </div>
    </td>
    <td>
        <div>
            ${feature.properties.population}
        </div>
    </td>
</tr>
    </tbody>
    </table>
    `;


    layer.bindPopup(popup);
}

function onEachFeature(feature, layer) {
    layer.on('click',  (e)  => {
        console.log(feature.properties.popupContent);
    });
}

slider.addEventListener("input", (e,v) => {

    let ul = document.getElementById("list-labels");
    let items = ul.getElementsByTagName("li");
    let decade = document.getElementById("timelineRange").value;

    handleSlidingbar(decade)
        .catch(err => console.log(err));

    map.closePopup();

    for (let i = 0; i < items.length; i++) {
        if (i == decade) {
            if (items[i].classList.contains("desactived-value")) items[i].classList.remove("desactived-value");
            items[i].classList.add("actived-value");
        } else {
            if (items[i].classList.contains("actived-value")) items[i].classList.remove("actived-value");
            items[i].classList.add("desactived-value");
        }
    }
});

function getActualSlidingbarValue() {
    return output.textContent;
}

async function handleHeat(year){
    return new Promise((resolve, reject) => {
        try {
            const heatArray = [];
            const decade = year;
            familyData.locais.forEach((element, index) => {
                heatArray.push([element.latitude, element.longitude, familyData.quantidates[index][decade]]);
            });
            return resolve(heatArray);
        } catch (err) {
            return reject(Error(err));
        }
    });
}


async function handleSlidingbar(year) {
    if (familyData === null || familyData === undefined) {
        alert("Escolha a família");
    } else {
        const data = await handleData(year, familyData);
        heat = L.heatLayer(data, layerCfg).addTo(map);
        if (control === false) {
           layerPontos
                .addLayer( L.geoJSON(allDotsStructure, {
                    onEachFeature: onEachDot,
                    pointToLayer:  (feature, latlng) => {
                        return L.circleMarker(latlng, {
                            fill: true,
                            fillColor: "#ffffff",
                            fillOpacity: 1,
                            color: "#a96f00",
                            radius: 5
                        })
                    }
                }))
                .addTo(map);
            control = true;
        }
    }
}

function handleData(year, familyData) {
    map.removeLayer(heat);
    return new Promise((resolve, reject) => {
        try {
        
            const heatArray = [];
            familyData.forEach((element, index) => {
                const dotsStructure = {
                    "type": "Feature",
                    "properties": {
                        "name": element.place,
                        "popupContent": element.place + " - " + element.city,
                        "city": element.city,
                        "population" : element.totalPopulation
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [element.longitude, element.latitude]
                    }
                };
                allDotsStructure.features.push(dotsStructure);
                heatArray.push([element.latitude, element.longitude, element.population[year]]);
            }
            );
            return resolve(heatArray);
        } catch (err) {
            return reject(Error(err));
        }
    });
}

let removeMarkers =  ()  => {
    map.removeLayer(heat);
    map.eachLayer( (layer) => {
        if (layer.feature !== undefined) {
            if (layer.feature.geometry.type === "Point") {
                map.removeLayer(layer);
            }
        }
    });
    allDotsStructure = {
        "type": "FeaturesCollection",
        "features": []
    };
    familyData = 0;
    heat = L.heatLayer();
    layerPontos = L.layerGroup().addTo(map);
    control = false;
};