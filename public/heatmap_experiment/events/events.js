// Handling Event Functions 
//  
let i = 0;


function openModal() {
    $('#myModal').modal('show');
    $('.map-search-button').addClass( "m-fadeOut" );
    $('.map-search-button').removeClass( "m-fadeIn" );
    $('#list-labels').addClass( "m-fadeOut" );
    $('#list-labels').removeClass( "m-fadeIn" );
    $('#timelineRange').addClass( "m-fadeOut" );
    $('#timelineRange').removeClass( "m-fadeIn" );
    $('#card-fade-name').addClass( "m-fadeOut" );
    $('#card-fade-name').removeClass( "m-fadeIn" );
    card.classList.remove("m-fadeIn");
    card.classList.toggle("m-fadeOut");
    map.removeLayer(heat);
} 

function closeModal() {
    $('#myModal').modal('hide');
    $('.map-search-button').addClass( "m-fadeIn" );
    $('.map-search-button').removeClass( "m-fadeOut" );
    $('#list-labels').addClass( "m-fadeIn" );
    $('#list-labels').removeClass( "m-fadeOut" );
    $('#timelineRange').addClass( "m-fadeIn" );
    $('#timelineRange').removeClass( "m-fadeOut" );
    $('#card-fade-name').addClass( "m-fadeOut" );
    $('#card-fade-name').removeClass( "m-fadeIn" );
    if($("#card-fade-name").hasClass("m-fadeOut")){
    cardName.classList.remove("m-fadeOut");
    cardName.classList.toggle("m-fadeIn");
    }

    if($("#card-fade-family").hasClass("m-fadeOut")){
        card.classList.remove("m-fadeOut");
        card.classList.toggle("m-fadeIn");
    }
}

function handleRFID(event){
    $("#search-family-rfid").focus();
}

function isModalOpen() {
    return !!$('#myModal').hasClass('in');
}

function removeLeafletGadgets() {
    $('.leaflet-bottom').remove();
    $('.leaflet-right').remove();
    $('.leaflet-control-container').remove();
}

function sendFamilyRfid(family) {
    if (!isModalOpen())
        openModal();
    familyName = family;
    $('#search-family').val('');
    $('#search-family').prop("disabled", true);
    $('#submit-form-search').attr("disabled", true);
    myKeyboard.setInput('');
    textLoop();
    i = 0;
}
// Events
$(document).ready(removeLeafletGadgets);
$(window).on('load', openModal);
$(window).on('keydown',handleRFID);


