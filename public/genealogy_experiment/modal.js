$(document).ready(() => {
  $("#myModal").modal("show");
  $("#personSelectModal").css("visibility", "hidden");
  $("#search-form-button").addClass("m-fadeOut");
  $("#search-back-button").removeClass("m-fadeOut");
  $("#card-fade-back").addClass("m-fadeOut");

  for (let a = 1700; a <= 1920; a++) {
    $("#yearPicker").append("<option>" + a + "</option>");
  }
});
