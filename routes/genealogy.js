const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const fs = require("fs");
const axios = require("axios");
const root_dir = process.cwd();
const dataManagerUrl = process.env.DATA_MANAGER_URL || process.env.DEFAULT_DM_URL;

router.get("/", (req, res) => {
    console.log("get family genealogy realizado");
    res.render(root_dir + "/src/genealogytree/index.html");
});


router.post("/family/",(req, res) => {
    console.log(req.body)
    if(req.body.year === '' && req.body.place === ''){
        console.log("APenas nome")
        axios
        .get(dataManagerUrl + "/search/?q=" + encodeURI(req.body.surname))
        .then(response => {
            if(response){
                res.send(response.data)
        }else{
            const json = {
                data: "Family Not Found"
            };
        }
        })
        .catch(err => res.send(err));
    }else if(req.body.place === ''){
        console.log("APenas nome e Ano")
        axios
        .get(dataManagerUrl + "/search/?q=" + encodeURI(req.body.surname) + "&y=" + encodeURI(req.body.year))
        .then(response => {
            if(response){
                res.send(response.data)
        }else{
            const json = {
                data: "Family Not Found"
            };
        }
        })
        .catch(err => res.send(err));
    }else if (req.body.year === ''){
        console.log("APenas nome e Local")
        axios
        .get(dataManagerUrl + "/search/?q=" + encodeURI(req.body.surname) + "&l=" + encodeURI(req.body.place))
        .then(response => {
            if(response){
                res.send(response.data)
        }else{
            const json = {
                data: "Family Not Found"
            };
        }
        })
        .catch(err => res.send(err));

    }else{
        console.log("Tudo")
        axios
        .get(dataManagerUrl + "/search/?q=" + encodeURI(req.body.surname) + "&y=" + encodeURI(req.body.year) + "&l=" + encodeURI(req.body.place))
        .then(response => {
            if(response){
                res.send(response.data)
        }else{
            const json = {
                data: "Family Not Found"
            };
        }
        })
        .catch(err => res.send(err));
    }
});

router.get("/id/:id",(req, res) => {
    axios
    .get(dataManagerUrl + "/tree/?id=" + encodeURI(req.params.id))
    .then(response => {
    if(response){
            res.send(response.data)
    }else{
        const json = {
            data: "Family Not Found"
        };
    }
    })
    .catch(err => res.send(err));
    
});

module.exports = router;
