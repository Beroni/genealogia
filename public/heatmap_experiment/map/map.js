// let bounds = new L.LatLngBounds(new L.LatLng(-5.202283, -31.7818401), new L.LatLng(-6.394034, -51.8866731));
let map = new L.Map('map-id', {
    minZoom: 9,
    maxZoom: 11,
    continuousWorld : false,
    noWrap: true
});

$(document).ready(function () {
    configMap();
});


function configMap() {
    let imageUrl = './heatmap_experiment/img/mapa_ceara.png';

    let  southWest = L.latLng(-8.4615,-34.7717);
    let northEast = L.latLng(-1.9881,-44.38485);
    imageBounds = [[-1.9881,-44.3848], [-8.4615,-34.7717]];
    bounds = L.latLngBounds(southWest, northEast);
    map.setView({
        lat: -6.7497768,
        lng: -39.0271646
            }, 9); 
    map.setMaxBounds(bounds);
    L.imageOverlay(imageUrl, imageBounds).addTo(map)
    
  

}
