## Museu da Genealogia

Crie um arquivo ".env". Depois de criado o arquivo utilize as seguintes variáveis de ambiente para iniciar o projeto.

```bash
APP_PORT=<node-app-port>
```

Para buildar o projeto:
```bash
sudo docker-compose build
```
Para subir o projeto:
```bash
sudo docker-compose up
```
Para subir e buildar o projeto ao mesmo tempo(Recomendado):
```bash
sudo docker-compose up --build
```

