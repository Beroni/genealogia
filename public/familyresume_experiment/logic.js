
let yearPicker = document.querySelector("#yearPicker");


$(document).ready(function () {
    $('#searchModal').modal('show');
    $('#infoModal').addClass('hidden');
    $(window).on('keydown',handleRFID);
});


// Keyboard
const $win = $(window);
const $box1 = $("#keyboard-container");
const $box2 = $("#search-form");

const pt_br = {
    'default': [
        '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
        '{tab} q w e r t y u i o p ´ [',
        '{lock} a s d f g h j k l ç ~ ] {enter}',
        '{shift} z x c v b n m , . ; / {shift}',
        '.com @ {space}'
    ],
    'shift': [
        '" ! @ # $ % ¨ & * ( ) _ + {bksp}',
        '{tab} Q W E R T Y U I O P ` {',
        '{lock} A S D F G H J K L Ç ^ } {enter}',
        '{shift} Z X C V B N M < > : ? {shift}',
        '.com @ {space}'
    ]
};


let Keyboard = window.SimpleKeyboard.default;

let myKeyboard = new Keyboard({
    onChange: input => onChange(input),
    onKeyPress: button => onKeyPress(button),
    physicalKeyboardHighlight: true,
    physicalKeyboardHighlightTextColor: "white",
    layout: pt_br
});

document.querySelector(".input").addEventListener("input", event => {
    myKeyboard.setInput(event.target.value);
});

$win.on("click.Bst",  (event) => {
    if ($box1.has(event.target).length === 0 && !$box1.is(event.target) &&
        $box2.has(event.target).length === 0 && !$box2.is(event.target)) {
        $("#keyboard-container").css("visibility", 'hidden');
    }
});

$("#search-family").click( () => {
    $("#keyboard-container").css("visibility", 'visible');
});

function onChange(input) {
    let text = myKeyboard.getInput();
    let lastLetter = text.charAt(text.length - 1);
    let penultimateLetter = text.charAt(text.length - 2);
    
    switch (penultimateLetter) {
        case '~':
            if (lastLetter === "a") handleAccent("ã");
            else if (lastLetter === "e") handleAccent("ẽ");
            else if (lastLetter === "i") handleAccent("ĩ");
            else if (lastLetter === "o") handleAccent("õ");
            else if (lastLetter === "u") handleAccent("ũ");
            else if (lastLetter === "A") handleAccent("Ã");
            else if (lastLetter === "E") handleAccent("Ẽ");
            else if (lastLetter === "I") handleAccent("Ĩ");
            else if (lastLetter === "O") handleAccent("Õ");
            else if (lastLetter === "U") handleAccent("Ũ");
            break;
        case '´':
            if (lastLetter === "a") handleAccent("á");
            else if (lastLetter === "e") handleAccent("é");
            else if (lastLetter === "i") handleAccent("í");
            else if (lastLetter === "o") handleAccent("ó");
            else if (lastLetter === "u") handleAccent("ú");
            else if (lastLetter === "A") handleAccent("Á");
            else if (lastLetter === "E") handleAccent("É");
            else if (lastLetter === "I") handleAccent("Í");
            else if (lastLetter === "O") handleAccent("Ó");
            else if (lastLetter === "U") handleAccent("Ú");
            break;
        case '^':
            if (lastLetter === "a") handleAccent("â");
            else if (lastLetter === "e") handleAccent("ê");
            else if (lastLetter === "i") handleAccent("î");
            else if (lastLetter === "o") handleAccent("ô");
            else if (lastLetter === "u") handleAccent("û");
            else if (lastLetter === "A") handleAccent("Â");
            else if (lastLetter === "E") handleAccent("Ê");
            else if (lastLetter === "I") handleAccent("Î");
            else if (lastLetter === "O") handleAccent("Ô");
            else if (lastLetter === "U") handleAccent("Û");
            break;
        case '`':
            if (lastLetter === "a") handleAccent("à");
            else if (lastLetter === "e") handleAccent("è");
            else if (lastLetter === "i") handleAccent("ì");
            else if (lastLetter === "o") handleAccent("ò");
            else if (lastLetter === "u") handleAccent("ù");
            else if (lastLetter === "A") handleAccent("À");
            else if (lastLetter === "E") handleAccent("È");
            else if (lastLetter === "I") handleAccent("Ì");
            else if (lastLetter === "O") handleAccent("Ò");
            else if (lastLetter === "U") handleAccent("Ù");
            break;
        case '¨':
            if (lastLetter === "a") handleAccent("ä");
            else if (lastLetter === "e") handleAccent("ë");
            else if (lastLetter === "i") handleAccent("ï");
            else if (lastLetter === "o") handleAccent("ö");
            else if (lastLetter === "u") handleAccent("ü");
            else if (lastLetter === "A") handleAccent("Ä");
            else if (lastLetter === "E") handleAccent("Ë");
            else if (lastLetter === "I") handleAccent("Ï");
            else if (lastLetter === "O") handleAccent("Ö");
            else if (lastLetter === "U") handleAccent("Ü");
            break;
        default:
            document.querySelector(".input").value = input;
            break;
    }

}

function onKeyPress(button) {
    if (button === "{shift}" || button === "{lock}") handleShift();
    if (button === "{bksp}") {
        let text = myKeyboard.getInput();
        let newText = text.substring(0, text.length - 1);
        myKeyboard.setInput(newText);
        document.querySelector(".input").value = newText
    }
    if (button === "{enter}") {
        handleFamilyName($('input[name=familyname]').val());
        $("#search-family").val("");
    }

}

async function handleFamilyName(name) {
        console.log(family.surnames.includes(name))
        console.log(name)
        if(family.surnames.includes(name)){
        let info = await getInfo(name);
        handleInfo(info);
        }

}
function handleAccent(accent) {
    let text = myKeyboard.getInput();
    let newText = text.substring(0, text.length - 2) + accent;
    myKeyboard.setInput(newText);
    document.querySelector(".input").value = newText;
}

function handleShift() {
    let currentLayout = myKeyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";
    myKeyboard.setOptions({
        layoutName: shiftToggle
    });  
}

async function getFamilyNamebyRFID(tag){
    return new Promise((resolve, reject) => {
        try {
                $.ajax({
                    type: 'GET',
                    url: '/rfid/'+encodeURI(String(tag)),
                    dataType: 'json',
                    success: (res) => {

                        return resolve(res.name);
                    },
                    error:  (err) => {
                        return reject(Error(err));
                    }
                });
        } catch (err) {
            return reject(Error("Error"));
        }
    });
}

function rfidEnter(event){
    let char = event.which || event.keyCode;
    console.log(char)
    if (char === 13) {
        event.preventDefault();

        var a = $('input[name=familynamerfid]').val()
        getFamilyNamebyRFID(a)
            .then(f_name =>{ 
                console.log(f_name)
                handleFamilyName(f_name);
                $("#search-family-rfid").val("");
            })
            .catch((err) => console.log(err));
    }
}

function handleRFID(event){
    $("#search-family-rfid").focus();
}

// Autocomplete
const family = {
    surnames: [
        "Abreu",
        "Afonso",
        "Aguiar",
        "Aires",
        "Albuquerque",
        "Alcântara",
        "Alcoforado",
        "Alecar",
        "Aleixo",
        "Alencar",
        "Alexandre",
        "Alexandria",
        "Alexandrina",
        "Almeida",
        "Alvarenga",
        "Alves",
        "Amâncio",
        "Amaral",
        "Amaro",
        "Amazonas",
        "Amor",
        "Amorim",
        "Amparo",
        "Andrade",
        "Angelim",
        "Anjo",
        "Anjos",
        "Antero",
        "Antônio",
        "Antunes",
        "Anunciação",
        "Apóstolo",
        "Aquino",
        "Aranha",
        "Araripe",
        "Araújo",
        "Arraes",
        "Arruda",
        "Assis",
        "Assunção",
        "Augusto",
        "Avelar",
        "Avelino",
        "Ayres",
        "Azevedo",
        "Balbino",
        "Bandeira",
        "Banhos",
        "Barbalho",
        "Barbosa",
        "Barreto",
        "Barros",
        "Barroso",
        "Basto",
        "Bastos",
        "Batalha",
        "Batista",
        "Belém",
        "Benedita",
        "Benedito",
        "Benício",
        "Bento",
        "Bernardes",
        "Bernardino",
        "Bernardo",
        "Bertoldo",
        "Bezerra",
        "Bispo",
        "Boa",
        "Boaventura",
        "Bombaça",
        "Bonfim",
        "Borge",
        "Borges",
        "Borja",
        "Botelho",
        "Braga",
        "Brandão",
        "Brás",
        "Brasil",
        "Braz",
        "Brígido",
        "Brito",
        "Buriti",
        "Cabral",
        "Cadeira",
        "Caetano",
        "Caieira",
        "Cajueiro",
        "Calasso",
        "Caldas",
        "Calheiro",
        "Calixto",
        "Calou",
        "Câmara",
        "Camilo",
        "Caminha",
        "Campos",
        "Cândida",
        "Cândido",
        "Canuto",
        "Capibaribe",
        "Cardoso",
        "Carmo",
        "Carneiro",
        "Carvalho",
        "Cassiano",
        "Castilho",
        "Castro",
        "Cavalcante",
        "Celestino",
        "Chagas",
        "Chaves",
        "Cidade",
        "Cipriano",
        "Ciriaco",
        "Cirino",
        "Clemente",
        "Clementino",
        "Coelho",
        "Colaço",
        "Constantino",
        "Coração",
        "Cordeiro",
        "Correia",
        "Costa",
        "Coutinho",
        "Couto",
        "Crispim",
        "Crus",
        "Cruz",
        "Cunegundes",
        "Cunha",
        "Custódio",
        "Damasceno",
        "Daniel",
        "Dantas",
        "Delfino",
        "Delgado",
        "Deus",
        "Dias",
        "Dimas",
        "Diniz",
        "DiogInês",
        "Dionísio",
        "Divino",
        "Domingos",
        "Dondon",
        "Dores",
        "Dourado",
        "Duarte",
        "Duhetes",
        "Dutra",
        "Encarnação",
        "Espírito",
        "Estácio",
        "Estevão",
        "Estrela",
        "Eusébio",
        "Eustáquio",
        "Evangelista",
        "Facundes",
        "Falcão",
        "Farias",
        "Faustino",
        "Favela",
        "Fé",
        "Feijó",
        "Feitosa",
        "Felipe",
        "Felispátria",
        "Félix",
        "Felizardo",
        "Fernandes",
        "Ferraz",
        "Ferreira",
        "Ferrer",
        "Ferro",
        "Figueiredo",
        "Filgueira",
        "Filgueiras",
        "Filho",
        "Firmino",
        "Firmo",
        "Fiuza",
        "Flores",
        "Fonseca",
        "Fontes",
        "Fortunato",
        "Fragoso",
        "França",
        "Francelina",
        "Francisco",
        "Franco",
        "Frazão",
        "Freire",
        "Freitas",
        "Frutuoso",
        "Furtado",
        "Gadelha",
        "Galdino",
        "Galiza",
        "Galvão",
        "Gama",
        "Garcia",
        "Garros",
        "Gentil",
        "Germano",
        "Glória",
        "Goiabeira",
        "Góis",
        "Gomes",
        "Gonçalves",
        "Gondim",
        "Gonzaga",
        "Gouveia",
        "Graça",
        "Grangeiro",
        "Granjeiro",
        "Gregório",
        "Guabiraba",
        "Guedes",
        "Guerra",
        "Guimarães",
        "Gusmão",
        "Henrique",
        "Holanda",
        "Homem",
        "Honorato",
        "Hora",
        "Jacó",
        "Jesus",
        "Joaquim",
        "Joaquina",
        "Jorge",
        "José",
        "Julião",
        "Júnior",
        "Lacerda",
        "Ladeira",
        "Landim",
        "Laurinda",
        "Lavor",
        "Leandro",
        "Leão",
        "Ledo",
        "Leitão",
        "Leite",
        "Lemos",
        "Leonardo",
        "Leonel",
        "Liberdade",
        "Lima",
        "Limeira",
        "Linhares",
        "Lins",
        "Lira",
        "Lisboa",
        "Lobo",
        "Lopes",
        "Lourença",
        "Lourenço",
        "Lucena",
        "Lustosa",
        "Luz",
        "Macambira",
        "Macedo",
        "Machado",
        "Maciel",
        "Madalena",
        "Madeira",
        "Magalhães",
        "Maia",
        "Maior",
        "Malheiro",
        "Malheiros",
        "Malvino",
        "Mangueira",
        "Maniges",
        "Manoel",
        "Marcionis",
        "Maria",
        "Mariel",
        "Marinheiro",
        "Marinho",
        "Mariz",
        "Marques",
        "Martins",
        "Mascarenhas",
        "Mata",
        "Matias",
        "Matos",
        "Máximo",
        "Medeiro",
        "Medeiros",
        "Medina",
        "Meireles",
        "Melo",
        "Mendes",
        "Mendonça",
        "Menezes",
        "Mercês",
        "Mesquita",
        "Militão",
        "Miranda",
        "Moita",
        "Mombaça",
        "Monta",
        "Monte",
        "Monteiro",
        "Montes",
        "Morais",
        "Moreira",
        "Moreno",
        "Morte",
        "Mota",
        "Moura",
        "Mumbaça",
        "Muniz",
        "Murici",
        "Nascimento",
        "Natividade",
        "Nazaré",
        "Nazarete",
        "Negrão",
        "Neri",
        "Neves",
        "Nicácio",
        "Nobre",
        "Nobrega",
        "Nogueira",
        "Nonato",
        "Noronha",
        "Nunes",
        "Ó",
        "OIiveira",
        "Olinda",
        "Oliveira",
        "Onofre",
        "osta",
        "Pacheco",
        "Pacífico",
        "Paes",
        "Paiva",
        "Paixão",
        "Palácio",
        "Palhano",
        "Palhares",
        "Pantaleão",
        "Parente",
        "Parnaíba",
        "Passos",
        "Patrício",
        "Patrocínio",
        "Paula",
        "Paulino",
        "Paulo",
        "Paz",
        "Pedro",
        "Pedrosa",
        "Pedroso",
        "Peixoto",
        "Pena",
        "Penha",
        "Pereira",
        "Perpétua",
        "Pessoa",
        "Piancó",
        "Piedade",
        "Pimentel",
        "Pinheiro",
        "Pinho",
        "Pinto",
        "Pires",
        "Pontes",
        "Porfírio",
        "Povos",
        "Prado",
        "Praxedes",
        "Prazeres",
        "Primo",
        "Purificação",
        "Quaresma",
        "Queiroz",
        "Quirino",
        "Rabelo",
        "Raimundo",
        "Ramalho",
        "Ramos",
        "Rangel",
        "Raposo",
        "Ravier",
        "Rego",
        "Reis",
        "Ressurreição",
        "Rezende",
        "Ribeiro",
        "Ricardo",
        "Ricarte",
        "Roberto",
        "Rocha",
        "Rodrigues",
        "Roldão",
        "Rolim",
        "Romão",
        "Romeiro",
        "Rosa",
        "Rosário",
        "Rozena",
        "Rufino",
        "Sá",
        "Sabino",
        "Sacramento",
        "Sales",
        "Salvador",
        "Sampaio",
        "Sancho",
        "Santa",
        "Santana",
        "Santiago",
        "Santo",
        "Santos",
        "São",
        "Saraiva",
        "Sátiro",
        "Sena",
        "Senhor",
        "Senhora",
        "Serafim",
        "Sergino",
        "Severino",
        "Siebra",
        "Silva",
        "Silveira",
        "Silvestre",
        "Simão",
        "Simões",
        "Simplício",
        "Siqueira",
        "Soares",
        "Sobral",
        "Sobreira",
        "Soledade",
        "Sousa",
        "Souto",
        "Sucupira",
        "Tal",
        "Taumaturgo",
        "Tavares",
        "Taveira",
        "Teixeira",
        "Teles",
        "Teófilo",
        "Teresa",
        "Tomás",
        "Torquato",
        "Torres",
        "Trajano",
        "Trindade",
        "Vale",
        "Valentim",
        "Vandeles",
        "Vasconcelos",
        "Vaz",
        "Veloso",
        "Ventura",
        "Veras",
        "Verdelina",
        "Veríssimo",
        "Viana",
        "Vidal",
        "Vieira",
        "Virgem",
        "Virgens",
        "Vital",
        "Vitoriano",
        "Xavier"
    ]
};


$("#search-family").autocomplete({
    source: family.surnames
});


$(".hg-button").on('click',() => {
    $("#search-family").autocomplete("search",  myKeyboard.getInput());
});

