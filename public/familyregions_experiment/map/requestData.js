
async function adjustingData(regions){
    return new Promise((resolve, reject) => {

       region_data=regions;
            region_data.features.forEach((obj,index)=>{


                setTimeout(function(){

                    $.ajax({
                        type: 'GET',
                        url: '/regions/'+encodeURI(obj.properties['name']),
                        dataType: 'json',
                        success:  (res) => {
    
                            console.log(res)
                            if (res.data.totalPopulation === null || res.data.totalPopulation===0){
                                obj.properties['totalPopulation'] = 1;
                            }else{
                                obj.properties['totalPopulation'] = res.data.totalPopulation;
                            }
    
                            if(res.data.totalFamilies === null || res.data.totalFamilies===0 ){
                                obj.properties['totalFamilies'] = 1;
                            }else{
                                obj.properties['totalFamilies'] = res.data.totalFamilies;
                            }
    
                            if( res.data.largest=== null){
                                obj.properties['largest'] = {"surname":"-","count":1,"year":"-"}
                            }else{
                                obj.properties['largest'] = res.data.largest;
                            }
    
                            if( res.data.earliest === null){
                                obj.properties['earliest'] = {"surname":"-","count":1,"year":"-"};
                            }else{
                                obj.properties['earliest'] = res.data.earliest;
                            }
    
                            if(res.data.latest === null){
    
                                obj.properties['latest'] = {"surname":"","count":1,"year":"-"};
                            }else{
                                obj.properties['latest'] = res.data.latest;
                            }
    
                            if(index == region_data.features.length - 1){
                                console.log(index)
                                return resolve(region_data);
                            }
    
    
                        },
                        error:  () => {
                            reject(Error(err));
                        }
                    });

                    
                    
                },index*200)

             });

    });
};


