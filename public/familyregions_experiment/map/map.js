// let bounds = new L.LatLngBounds(new L.LatLng(-5.202283, -31.7818401), new L.LatLng(-6.394034, -51.8866731));
let map = new L.Map('map-id', {
    minZoom: 9,
    maxZoom: 11,
    continuousWorld : false,
    noWrap: true
});

function eachFeture(feature, layer) {
    // does this feature have a property named popupConten
    layer.on({
        mouseover:highlightDot,
        mouseout: resetDotHighlight
    }
    )
    console.log(feature.properties)

   let popup = `
    <table  style="width:900px;">
        <tbody>
            <tr>
                <td>
                    <div>
                        <b> Nome do Local: </b>
                    </div>
                </td>
                <td>
                    <div>    
                        ${feature.properties.name}
                    </div>
                </td>
            </tr>
            <tr>
            <td>
                <div>
                    <b>População Total: </b>
                </div>
            </td>
            <td>
                <div>    
                    ${feature.properties.totalPopulation}
                </div>
            </td>
        </tr>
        <tr>
        <td>
            <div>
                <b>Quantidade de Famílias:</b>
            </div>
        </td>
        <td>
            <div>    
                ${feature.properties.totalFamilies}
            </div>
        </td>
    </tr>
        <tr>
        <td>
            <div>
                <b>Família Mais Antiga:</b>
            </div>
        </td>
        <td>
            <div>    
                ${feature.properties.earliest.surname}  - ${feature.properties.earliest.year}
            </div>
        </td>
    </tr>
        <tr> 
        <td>
            <div>
                <b>Família Mais Nova:</b>
            </div>
        </td>
        <td>
            <div>    
                ${feature.properties.latest.surname}  - ${feature.properties.latest.year}
            </div>
        </td>
    </tr>
        <tr>
        <td>
            <div>
                <b>Família Mais Populosa:</b>
            </div>
        </td>
        <td>
            <div>    
                ${feature.properties.largest.surname}  - ${((feature.properties.largest.count/feature.properties.totalPopulation) * 100).toFixed(2)}%
            </div>
        </td>
    </tr>
        </tbody>
    </table>
    `
    console.log(feature.properties.largest.count)

    layer.bindPopup(popup);
}


$(document).ready( function () {
    configMap();
     adjustingData(regions)
     .then(result =>  {
        L.geoJSON(result, { onEachFeature: eachFeture, style: myStyle }).addTo(map);
        map.flyTo([ -6.7492605,-38.9735119], 10);
})
     .catch(err => console.log(err));

});


function configMap() {
    let imageUrl = './heatmap_experiment/img/mapa_ceara.png',
    imageBounds = [[-1.9881,-44.3848], [-8.4615,-34.7717]];
    let  southWest = L.latLng(-8.4615,-34.7717);
    let northEast = L.latLng(-1.9881,-44.38485);
    bounds = L.latLngBounds(southWest, northEast);
    map.setView({
        lat: -6.7497768,
        lng: -39.0271646
            }, 9); 
    map.setMaxBounds(bounds);
    L.imageOverlay(imageUrl, imageBounds).addTo(map)
}
