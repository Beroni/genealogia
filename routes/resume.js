const express = require("express");
const router = express.Router();
const bodyParser = require('body-parser');
const root_dir = process.cwd();
const axios = require("axios");
const dataManagerUrl = process.env.DATA_MANAGER_URL || process.env.DEFAULT_DM_URL;


router.get('/',  (req, res) => {
    console.log("get family regions realizado");
    res.render(root_dir + '/src/familyresume/index.html');
});


router.get('/family/:surname',(req,res) =>{
    axios
    .get(dataManagerUrl + "/details/?surname=" + encodeURI(req.params.surname))
    .then(response => {
        if(response){
        res.send({
            data: response.data
        })
    }else{
        const json = {
            data: "Family Not Found"
        };
    }
    })
    .catch(err => res.send(err));

})

module.exports = router;
