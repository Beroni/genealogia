const $win = $(window);
const $box1 = $("#keyboard-container");
const $box2 = $("#search-form");
const card = document.getElementById("card-fade-family");
const cardName = document.getElementById("card-fade-name");
let familyData;

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

$win.on("click.Bst",  (event) => {
    if ($box1.has(event.target).length === 0 && !$box1.is(event.target) &&
        $box2.has(event.target).length === 0 && !$box2.is(event.target)) {
        $("#keyboard-container").css("visibility", 'hidden');

    }
});

$("#search-family").click( () => {
    $("#keyboard-container").css("visibility", 'visible');
});



$("#submit-form-search").click( (event) => {
    event.preventDefault();
    $("submit-form-search").attr("disabled", true);
    $("search-family").attr("disabled", true);
    if ($('input[name=familyname]').val() == "") {
        $("#search-form").addClass("has-error");
        $("#search-family").attr("placeholder", "Não deixe o nome da família em branco");
    } else {
        $("#keyboard-container").css("visibility", 'hidden');
        familyName = {
            family: $('input[name=familyname]').val()
        };
        loadingBar();
        sendFamilyName(familyName);
    }
});

$("#search-form-button").click( (event) => {
    openModal();
    removeMarkers();
});


function loadingBar() {
    $("#loading").html(`
    <div class="loader"></div>
    `)
}

function removeLoadingBar() {
    $('#loading').empty();
}

function sendFamilyName(familyName) {
    console.log(familyName);
    $.ajax({
        type: 'GET',
        url: '/heatmap/family/'+familyName.family.toProperCase(),
        dataType: 'json',
        success:  (res) => {
            familyData = res.data;
            document.querySelector(".input").value = "";
            myKeyboard.setInput("");
            if (familyData === "Family Not Found") {
                $("#search-form").addClass("has-error");
                $("#search-family").attr("placeholder", "Digite o nome da família corretamente.");
            } else {

                $("#family-image").attr("src", "./heatmap_experiment/img/" + res.image);
                document.getElementById("timelineRange").disabled = false;
                handleSlidingbar(document.getElementById("timelineRange").value)
                    .then( () => {
                        document.getElementById("famile-name-card").innerHTML = "Família </br>" + familyName.family;
                        closeModal();
                        removeLoadingBar();
                    })
                    .catch(err => console.log(err));
            }
        },
        error:  () => {
            console.log("Error !")
        }

    });
}

document.getElementById("search-family").addEventListener("input",  (e) => {
    if ($("#search-form").hasClass("has-error")) {
        $("#search-form").removeClass("has-error");
        $("#search-family").attr("placeholder", "");
    }
});
 
// Autocomplete
const family = {
    surnames: [
        "Abreu",
        "Afonso",
        "Aguiar",
        "Aires",
        "Albuquerque",
        "Alcântara",
        "Alcoforado",
        "Alecar",
        "Aleixo",
        "Alencar",
        "Alexandre",
        "Alexandria",
        "Alexandrina",
        "Almeida",
        "Alvarenga",
        "Alves",
        "Amâncio",
        "Amaral",
        "Amaro",
        "Amazonas",
        "Amor",
        "Amorim",
        "Amparo",
        "Andrade",
        "Angelim",
        "Anjo",
        "Anjos",
        "Antero",
        "Antônio",
        "Antunes",
        "Anunciação",
        "Apóstolo",
        "Aquino",
        "Aranha",
        "Araripe",
        "Araújo",
        "Arraes",
        "Arruda",
        "Assis",
        "Assunção",
        "Augusto",
        "Avelar",
        "Avelino",
        "Ayres",
        "Azevedo",
        "Balbino",
        "Bandeira",
        "Banhos",
        "Barbalho",
        "Barbosa",
        "Barreto",
        "Barros",
        "Barroso",
        "Basto",
        "Bastos",
        "Batalha",
        "Batista",
        "Belém",
        "Benedita",
        "Benedito",
        "Benício",
        "Bento",
        "Bernardes",
        "Bernardino",
        "Bernardo",
        "Bertoldo",
        "Bezerra",
        "Bispo",
        "Boa",
        "Boaventura",
        "Bombaça",
        "Bonfim",
        "Borge",
        "Borges",
        "Borja",
        "Botelho",
        "Braga",
        "Brandão",
        "Brás",
        "Brasil",
        "Braz",
        "Brígido",
        "Brito",
        "Buriti",
        "Cabral",
        "Cadeira",
        "Caetano",
        "Caieira",
        "Cajueiro",
        "Calasso",
        "Caldas",
        "Calheiro",
        "Calixto",
        "Calou",
        "Câmara",
        "Camilo",
        "Caminha",
        "Campos",
        "Cândida",
        "Cândido",
        "Canuto",
        "Capibaribe",
        "Cardoso",
        "Carmo",
        "Carneiro",
        "Carvalho",
        "Cassiano",
        "Castilho",
        "Castro",
        "Cavalcante",
        "Celestino",
        "Chagas",
        "Chaves",
        "Cidade",
        "Cipriano",
        "Ciriaco",
        "Cirino",
        "Clemente",
        "Clementino",
        "Coelho",
        "Colaço",
        "Constantino",
        "Coração",
        "Cordeiro",
        "Correia",
        "Costa",
        "Coutinho",
        "Couto",
        "Crispim",
        "Crus",
        "Cruz",
        "Cunegundes",
        "Cunha",
        "Custódio",
        "Damasceno",
        "Daniel",
        "Dantas",
        "Delfino",
        "Delgado",
        "Deus",
        "Dias",
        "Dimas",
        "Diniz",
        "DiogInês",
        "Dionísio",
        "Divino",
        "Domingos",
        "Dondon",
        "Dores",
        "Dourado",
        "Duarte",
        "Duhetes",
        "Dutra",
        "Encarnação",
        "Espírito",
        "Estácio",
        "Estevão",
        "Estrela",
        "Eusébio",
        "Eustáquio",
        "Evangelista",
        "Facundes",
        "Falcão",
        "Farias",
        "Faustino",
        "Favela",
        "Fé",
        "Feijó",
        "Feitosa",
        "Felipe",
        "Felispátria",
        "Félix",
        "Felizardo",
        "Fernandes",
        "Ferraz",
        "Ferreira",
        "Ferrer",
        "Ferro",
        "Figueiredo",
        "Filgueira",
        "Filgueiras",
        "Filho",
        "Firmino",
        "Firmo",
        "Fiuza",
        "Flores",
        "Fonseca",
        "Fontes",
        "Fortunato",
        "Fragoso",
        "França",
        "Francelina",
        "Francisco",
        "Franco",
        "Frazão",
        "Freire",
        "Freitas",
        "Frutuoso",
        "Furtado",
        "Gadelha",
        "Galdino",
        "Galiza",
        "Galvão",
        "Gama",
        "Garcia",
        "Garros",
        "Gentil",
        "Germano",
        "Glória",
        "Goiabeira",
        "Góis",
        "Gomes",
        "Gonçalves",
        "Gondim",
        "Gonzaga",
        "Gouveia",
        "Graça",
        "Grangeiro",
        "Granjeiro",
        "Gregório",
        "Guabiraba",
        "Guedes",
        "Guerra",
        "Guimarães",
        "Gusmão",
        "Henrique",
        "Holanda",
        "Homem",
        "Honorato",
        "Hora",
        "Jacó",
        "Jesus",
        "Joaquim",
        "Joaquina",
        "Jorge",
        "José",
        "Julião",
        "Júnior",
        "Lacerda",
        "Ladeira",
        "Landim",
        "Laurinda",
        "Lavor",
        "Leandro",
        "Leão",
        "Ledo",
        "Leitão",
        "Leite",
        "Lemos",
        "Leonardo",
        "Leonel",
        "Liberdade",
        "Lima",
        "Limeira",
        "Linhares",
        "Lins",
        "Lira",
        "Lisboa",
        "Lobo",
        "Lopes",
        "Lourença",
        "Lourenço",
        "Lucena",
        "Lustosa",
        "Luz",
        "Macambira",
        "Macedo",
        "Machado",
        "Maciel",
        "Madalena",
        "Madeira",
        "Magalhães",
        "Maia",
        "Maior",
        "Malheiro",
        "Malheiros",
        "Malvino",
        "Mangueira",
        "Maniges",
        "Manoel",
        "Marcionis",
        "Maria",
        "Mariel",
        "Marinheiro",
        "Marinho",
        "Mariz",
        "Marques",
        "Martins",
        "Mascarenhas",
        "Mata",
        "Matias",
        "Matos",
        "Máximo",
        "Medeiro",
        "Medeiros",
        "Medina",
        "Meireles",
        "Melo",
        "Mendes",
        "Mendonça",
        "Menezes",
        "Mercês",
        "Mesquita",
        "Militão",
        "Miranda",
        "Moita",
        "Mombaça",
        "Monta",
        "Monte",
        "Monteiro",
        "Montes",
        "Morais",
        "Moreira",
        "Moreno",
        "Morte",
        "Mota",
        "Moura",
        "Mumbaça",
        "Muniz",
        "Murici",
        "Nascimento",
        "Natividade",
        "Nazaré",
        "Nazarete",
        "Negrão",
        "Neri",
        "Neves",
        "Nicácio",
        "Nobre",
        "Nobrega",
        "Nogueira",
        "Nonato",
        "Noronha",
        "Nunes",
        "Ó",
        "OIiveira",
        "Olinda",
        "Oliveira",
        "Onofre",
        "Pacheco",
        "Pacífico",
        "Paes",
        "Paiva",
        "Paixão",
        "Palácio",
        "Palhano",
        "Palhares",
        "Pantaleão",
        "Parente",
        "Parnaíba",
        "Passos",
        "Patrício",
        "Patrocínio",
        "Paula",
        "Paulino",
        "Paulo",
        "Paz",
        "Pedro",
        "Pedrosa",
        "Pedroso",
        "Peixoto",
        "Pena",
        "Penha",
        "Pereira",
        "Perpétua",
        "Pessoa",
        "Piancó",
        "Piedade",
        "Pimentel",
        "Pinheiro",
        "Pinho",
        "Pinto",
        "Pires",
        "Pontes",
        "Porfírio",
        "Povos",
        "Prado",
        "Praxedes",
        "Prazeres",
        "Primo",
        "Purificação",
        "Quaresma",
        "Queiroz",
        "Quirino",
        "Rabelo",
        "Raimundo",
        "Ramalho",
        "Ramos",
        "Rangel",
        "Raposo",
        "Ravier",
        "Rego",
        "Reis",
        "Ressurreição",
        "Rezende",
        "Ribeiro",
        "Ricardo",
        "Ricarte",
        "Roberto",
        "Rocha",
        "Rodrigues",
        "Roldão",
        "Rolim",
        "Romão",
        "Romeiro",
        "Rosa",
        "Rosário",
        "Rozena",
        "Rufino",
        "Sá",
        "Sabino",
        "Sacramento",
        "Sales",
        "Salvador",
        "Sampaio",
        "Sancho",
        "Santa",
        "Santana",
        "Santiago",
        "Santo",
        "Santos",
        "São",
        "Saraiva",
        "Sátiro",
        "Sena",
        "Senhor",
        "Senhora",
        "Serafim",
        "Sergino",
        "Severino",
        "Siebra",
        "Silva",
        "Silveira",
        "Silvestre",
        "Simão",
        "Simões",
        "Simplício",
        "Siqueira",
        "Soares",
        "Sobral",
        "Sobreira",
        "Soledade",
        "Sousa",
        "Souto",
        "Sucupira",
        "Tal",
        "Taumaturgo",
        "Tavares",
        "Taveira",
        "Teixeira",
        "Teles",
        "Teófilo",
        "Teresa",
        "Tomás",
        "Torquato",
        "Torres",
        "Trajano",
        "Trindade",
        "Vale",
        "Valentim",
        "Vandeles",
        "Vasconcelos",
        "Vaz",
        "Veloso",
        "Ventura",
        "Veras",
        "Verdelina",
        "Veríssimo",
        "Viana",
        "Vidal",
        "Vieira",
        "Virgem",
        "Virgens",
        "Vital",
        "Vitoriano",
        "Xavier"
    ]
};


$("#search-family").autocomplete({
    source: family.surnames
});